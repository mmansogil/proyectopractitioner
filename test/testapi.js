require('dotenv').config();
const mocha = require('mocha');
const chai = require('chai');
const chaihttp = require('chai-http');
const mLabAPIKey = "?apiKey=" + process.env.MLAB_API_KEY;
chai.use(chaihttp);

var should = chai.should();

describe('Test Inicio Servidor',
  function(){
    it('Test practitioner ',function(done){
      chai.request('http://localhost:3000')
          .get('/practitioner/v1/hello')
          .end(
            function(err, res){
              // esta función sería la asercción
              console.log('Request has finished');
              console.log(err);
              res.should.have.status(200);
              res.body.msg.should.be.eql('API practitioner funcionando');
              done();
          }
        )
      }
    )
  }
)



describe('Test Conexión mongo',
  function(){
    it('Test mongo  ',function(done){
      chai.request('https://api.mlab.com')
          .get('/api/1/databases/apitechummg9ed/collections/' + mLabAPIKey)
          .end(
            function(err, res){
              // esta función sería la asercción
              console.log('Request has finished');
              console.log(err);
              res.should.have.status(200);
              res.should.be.json;
              res.body.should.be.a('array');
              done();
          }
        )
      }
    )
  }
)

describe('Recupera usuarios',
  function(){
    it('Test mongo  ',function(done){
      chai.request('http://localhost:3000')
          .get('/practitioner/v1/user')
          .end(
            function(err, res){
              // esta función sería la asercción
              console.log('Request has finished');
              console.log(err);
              res.should.have.status(200);
              res.should.be.json;
              res.body.should.be.a('array');
              done();
          }
        )
      }
    )
  }
)
