const requestJson = require('request-json');
const dateFormat = require('dateformat');
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechummg9ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
const util = require('../common/util');



function getAccountById(req,res){
  console.log("GET /practitioner/v1/account/:id");
    var httpClient = requestJson.createClient(baseMlabURL);
      console.log("Cliente Creado");
      var id = req.params.id;
        console.log("id de cuenta a traer" + id);
        var query = 'q={"userid":'+id+',"accountStatus"'+':true}';
        httpClient.get ("account?" + query + "&" + mLabAPIKey,
        function (err,resMLab,body)
        {
          if (err){
            var response = {
              "msg": "Error obtenien docuenta de usuario"
            }
          res.status(500);
          }else{
           if(body.length > 0){
              var response= body;
           }else{
              var response = {
                "msg" : "error cuenta"
            }
            res.status(404);
            }
          }
        res.send(response);
        }
      )
}

function createAccount(req,res){
  console.log("POST /practitioner/v1/account")
  var cty = req.body.country;
  var iban = util.generaIBAN(cty);
  var d = new Date();
  console.log(iban);
  var newAccount = {
    "IBAN": iban,
    "userid":req.body.userid,
    "country": cty,
    "currency": req.body.currency,
    "balance": Number (0),
    "createdate": dateFormat(new Date(), "yyyy-mm-dd h:MM:ss"),
    "accountStatus":true
  };
  var httpClient = requestJson.createClient(baseMlabURL);
  httpClient.post("account?" + mLabAPIKey,newAccount,
    function (err,resMlab,body)
    {
      var response = !err ? body : {
        "msg" : "Error creando cuenta"
      }
      res.status(201).send("Cuenta creada");
    }
  )
}

function deleteAccount(req,res){
  console.log("DELETE /practitioner/v1/account")
  var httpClient = requestJson.createClient(baseMlabURL);
    console.log("Cliente Creado");
    var userid = req.body.userid;
    var iban = req.body.IBAN;
    var updateAccount ={ "$set" : { "accountStatus" : false } };
    console.log("id de usuario a traer " + userid +" y el IBAN: "+iban);
    var query = 'q={"userid":'+userid+' , IBAN:"'+iban+'"}';
    httpClient.get ("account?" + query + "&" + mLabAPIKey,
      function (err,resMLab,body)
      {
        if (err){
          var response = {
            "msg": "Error obteniendo cuenta"
          }
        res.status(500);
        }else{
         if(body.length > 0){
           if (body[0].balance===0){
             httpClient = requestJson.createClient(baseMlabURL);
            httpClient.put("account?" +query+"&"+mLabAPIKey,updateAccount,
              function (err,resMlab,body)
              {
                var response = !err ? body : {
                  "msg" : "Error cerrando cuenta"
                }
                res.status(200).send("Cuenta cerrada");
              }
            )
            }
            else{
              var response = {
                "msg" : "No pude cerrar la cuenta su balance es:"+body[0].balance +" fecha:" + d
              }
            }
         }else{
            var response = {
              "msg" : "cuenta no encontrado"
          }
          res.status(404);
          }
        }

      }
    )

}





module.exports.getAccountById = getAccountById;
module.exports.createAccount = createAccount;
module.exports.deleteAccount = deleteAccount;
