const requestJson = require('request-json');
const dateFormat = require('dateformat');
const baseMlabURL = "https://api.mlab.com/api/1/databases/apitechummg9ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;


function getMovementAccount(req,res){
  console.log("GET /practitioner/v1/movement");

  var httpClient = requestJson.createClient(baseMlabURL);
    console.log("Cliente Mlab Creado");
    var query = 'q={ IBAN:"'+req.params.IBAN+'"}';
    console.log("recupera del body"+query);
    httpClient.get ("movement?"+query+"&"+mLabAPIKey,
      function (err,resMLab,body)
      {
        var response = !err ? body : {
          "msg" : "Error obteniendo movimientos"
        }
        res.send(response);
      }
    )

}


function createMovement(req,res){
  console.log("POST /practitioner/v1/movement");
  var httpClient = requestJson.createClient(baseMlabURL);
  console.log("Cliente Creado"+ req.body.IBAN);
  console.log("Cliente Mlab Creado "+req.body.balance );
  if (req.body.topt==="INGRESO"){
     balance = Number (req.body.balance) + Number(req.body.amount);

  }else{
    balance = Number (req.body.balance) + (Number(req.body.amount) * -1);
  }
  console.log("Balance claculado"+balance);
  var newMovement = {
    "IBAN" : req.body.IBAN,
    "cdate": dateFormat(new Date(), "yyyy-mm-dd h:MM:ss"),
    "topt": req.body.topt,
    "amount":req.body.amount,
    "balance":balance,
    "concept":req.body.concept
  }
  httpClient.post("movement?" + mLabAPIKey,newMovement,
    function (err,resMlab,body)
    {
      var response = !err ? body : {
        "msg" : "Error creando movimiento"
      }

      httpClient = requestJson.createClient(baseMlabURL);
      var query = 'q={ IBAN:"'+req.body.IBAN+'"}';

          var updateAccount ={ "$set" : { "balance" : balance } };
     httpClient.put("account?" +query+"&"+mLabAPIKey,updateAccount,
       function (err,resMlab,body)
       {
         var response = !err ? body : {
           "msg" : "Error actualizando cuenta"
         }
       }
     )
     res.status(201).send("Movimiento creado");
    }
  )

}


module.exports.getMovementAccount = getMovementAccount;
module.exports.createMovement = createMovement;
