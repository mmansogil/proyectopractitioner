const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechummg9ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
const crypt = require('../common/crypt');

function createUser(req, res){
  console.log("POST /practitioner/v1/user");
  httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente MLAB Creado");
  var query = 's={"id":-1}&l=1';

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
      var newUser = {
      "id":body[0].id+1,
      "first_name": req.body.first_name,
      "last_name": req.body.last_name,
      "email":req.body.email,
      "password":crypt.hash(req.body.password)
      };
      var httpClient = requestJson.createClient(baseMLabURL);
      httpClient.post("user?" + mLabAPIKey,newUser,
      function (err,resMLab,body)
      {
        var response = !err ? body : {
          "msg" : "Error creando usuario"
        }
        res.status(201).send("usuario creado");
      }
    )
    }
  )
}

function getUsers(req, res) {
  console.log("GET /practitioner/v1/user");
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente Mlab Creado");
  httpClient.get ("user?" + mLabAPIKey,
    function (err,resMLab,body)
    {
      var response = !err ? body : {
        "msg" : "Error obteniendo usuario"
      }
      res.send(response);
    }
  )
}


function getUserById(req, res){
  console.log("GET /practitioner/v1/user/:id");
  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente Mlab Creado");
  var id = req.params.id;
  console.log("id de usuario a traer" + id);
  var query = 'q={"id":'+id+'}';
  httpClient.get ("user?" + query + "&" + mLabAPIKey,
    function (err,resMLab,body)
    {
      if (err){
        var response = {
          "msg": "Error obteniendo usuario"
        }
      res.status(500);
      }else{
       if(body.length > 0){
          var response= body[0];
       }else{
          var response = {
            "msg" : "Usuario no encontrado"
        }
        res.status(404);
        }
      }
    res.send(response);
    }
  )
}

function deleteUser(req, res){
      console.log('DELETE /practitioner/v1/user/:_id');
      var httpClient = requestJson.createClient(baseMLabURL);
      console.log("Cliente Mlab Creado");

      var query = 'q={"id":'+req.params._id;+'}';
      httpClient.delete("user?" + query + "&" + mLabAPIKey,
        function (err,resMLab,body)
        {
          var response = !err ? body : {
            "msg" : "Error borrando usuario"
          }
          res.status(201).send("usuario borrando");
        }
    )

}


function UpdateUser(req, res){
      console.log('PUT /practitioner/v1/user/:_id');


}



module.exports.createUser = createUser;
module.exports.getUsers = getUsers;
module.exports.getUserById = getUserById;
module.exports.deleteUser = deleteUser;
module.exports.UpdateUser = UpdateUser;
