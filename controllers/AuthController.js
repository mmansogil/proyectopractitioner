const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechummg9ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
const crypt = require('../common/crypt')

function login(req, res) {
 console.log("POST /apitechu/v1/login");

 var query = 'q={"email": "' + req.body.email + '"}';
 console.log("query es " + query);

 httpClient = requestJson.createClient(baseMLabURL);
 httpClient.get("user?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {
     if (!body[0]) {
       var response = {
         "mensaje" : "Login incorrecto, email y/o passsword no encontrados"
       }
       res.status(401);
       res.send(response);
     } else{
     var isPasswordcorrect =
       crypt.checkPassword(req.body.password, body[0].password);
     console.log("Password correct is " + isPasswordcorrect);

     if (!isPasswordcorrect) {
       var response = {
         "mensaje" : "Login incorrecto, email y/o passsword no encontrados"
       }
       res.status(401);
       res.send(response);
     } else {
       console.log("Got a user with that email and password, logging in");
       query = 'q={"id" : ' + body[0].id +'}';
       console.log("Query for put is " + query);
       var putBody = '{"$set":{"logged":true}}';
       httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
         function(errPUT, resMLabPUT, bodyPUT) {
           console.log("PUT done");
           var response = {
             "msg" : "Usuario logado con éxito",
             "idUsuario" : body[0].id
           }
           res.send(response);
         }
       )
     }
   }
  }
   );

}

function logout(req, res) {
 console.log("POST /apitechu/v1/logout/:id");

 var httpClient = requestJson.createClient(baseMLabURL);
 console.log("Cliente Creado");
 var id = req.params.id;
 console.log("id de usuario a traer" + id);
 var query = 'q={"id":'+id+',"logged":true }';
  var putBody = '{"$unset":{"logged":""}}';
 httpClient.get ("user?" + query + "&" + mLabAPIKey,
   function (err,resMLab,body)
   {
     if (err){
       var response = {
         "msg": "Error obteniendo usuario"
       }

        res.send(response);
     }else{
      if(body.length > 0){
        httpClient.put ("user?" + query + "&" + mLabAPIKey,JSON.parse(putBody),
         function (MLerr,MLresMLab,MLbody){
           var response = {
             "msg" : "Usuario logout con éxito",
             "idUsuario" : body[0].id
           }
           res.send(response);
         }
       )
      }else{
         var response = {
           "msg" : "Usuario no encontrado"
       }
       res.status(404);

       }
     }

   }
 )
}





module.exports.login = login;
module.exports.logout = logout;
