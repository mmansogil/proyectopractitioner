const requestJson = require('request-json');
const randomstring = require("randomstring");


function generaIBAN (cty){
  var str = cty + genera(2)+genera(4)+genera(4)+genera(4)+genera(4)
  return str;
}

function genera(n){
  var str = " "+randomstring.generate({
    length: n,
    charset: 'alphanumeric',
    capitalization: 'uppercase'
  });
    return str;
}


module.exports.generaIBAN = generaIBAN;
