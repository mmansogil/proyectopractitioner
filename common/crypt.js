const bcrypt = require('bcrypt');
function hash(data){
  console.log("Hashing Data");
  return bcrypt.hashSync(data, 10);
}


function checkPassword(passwordFromUserInPlainText,passwodFromDBHashed){
  console.log("Checking Password");
  return bcrypt.compareSync(passwordFromUserInPlainText,passwodFromDBHashed);
}

module.exports.hash = hash;
module.exports.checkPassword = checkPassword;
