require('dotenv').config();
const express = require('express');
const app = express();
const port = process.env.PORT || 3000;
const authController = require('./controllers/AuthController');
const userController = require('./controllers/UserController');
const accountController = require('./controllers/AccountController');
const movementController = require('./controllers/MovementController');
const changeCurrencyController = require('./controllers/ChangeCurrencyController');
app.use(express.json());
var enableCORS = function(req,res,next){
  res.set("Access-Control-Allow-Origin","*");
  res.set("Access-Control-Allow-Methods","POST,GET,OPTIONS,DELETE,PUT");
  res.set("Access-Control-Allow-Headers","Content-Type");
  next();
}
app.use(enableCORS);
app.listen(port);
  console.log("API escuchando en el puerto" + port);
  app.get('/practitioner/v1/hello',
    function(req, res){
      console.log("GET /practitioner/v1/hello");
      res.send({"msg" : "API practitioner funcionando"});
    }
  )
  app.post('/practitioner/v1/login/',authController.login);
  app.post('/practitioner/v1/logout/:id',authController.logout);
  app.post('/practitioner/v1/user',userController.createUser);
  app.get('/practitioner/v1/user',userController.getUsers);
  app.get('/practitioner/v1/user/:id',userController.getUserById);
  app.delete('/practitioner/v1/user/:id',userController.deleteUser);
  app.put('/practitioner/v1/user/:id', userController.UpdateUser);
  app.get('/practitioner/v1/account/:id', accountController.getAccountById);
  app.post('/practitioner/v1/account',accountController.createAccount);
  app.delete('/practitioner/v1/account',accountController.deleteAccount);
  app.get('/practitioner/v1/movement/:IBAN',movementController.getMovementAccount);
  app.post('/practitioner/v1/movement',movementController.createMovement);
  app.post('/practitioner/v1/convert/', changeCurrencyController.convertCurrency);
